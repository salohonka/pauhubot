import * as Discord from "discord.js";

export default class DiscordClient extends Discord.default.Client {
    constructor() {
        super();

        this._voiceConn = null;
        this._voiceChannel = null;

        this.joinVoiceChannelFromMessage = this.joinVoiceChannelFromMessage.bind(this);
        this.joinVoiceChannel = this.joinVoiceChannel.bind(this);
        this.findSenderVoiceChannel = this.findSenderVoiceChannel.bind(this);
        this.leaveVoiceChannel = this.leaveVoiceChannel.bind(this);
        this.isVoiceConnected = this.isVoiceConnected.bind(this);
        this.senderHasRole = this.senderHasRole.bind(this);
        this.spamMessage = this.spamMessage.bind(this);
        this.getVoiceConnection = this.getVoiceConnection.bind(this);
        this.getVoiceChannel = this.getVoiceChannel.bind(this);
        this._pingTarget = this._pingTarget.bind(this);
        this.bindCommand = this.bindCommand.bind(this);

        this.on("ready", () => {
            console.log("PauhuBot ready");
        });
    }

    /**
     * Attempts to join a voice channel. Uses the rest of the command
     * as the name of the channel, or if "me" is given, joins the channel
     * of the requester
     * @param message The Discord.js message object that contains the command
     */
    joinVoiceChannelFromMessage(message) {
        let args = message.content.split(" ");
        let chanName = args[1];
        if (!chanName) {
            console.log("Invalid channel name");
            return;
        }

        // Requester wants bot to join their current channel
        if (chanName === "me") {
            this._voiceChannel = this.findSenderVoiceChannel(message);
            if (this._voiceChannel === null) {
                console.log("voice channel not found");
                throw "Channel not found!";
            }

            // Requester wants bot to join a specified channel on the server
        } else {
            this._voiceChannel = this.channels.filter(channel => channel.name === chanName).first();
            if (this._voiceChannel === undefined) {
                this._voiceChannel = null;
                throw "Channel not found!";
            }
        }

        return this.joinVoiceChannel(this._voiceChannel);
    }

    /**
     * Attempts to join a voice channel given as a parameter
     * @param voiceChannel Discord.js voicechannel object
     * @return Promise that resolves the Discord.js voice connection object
     */
    async joinVoiceChannel(voiceChannel) {
        if (!voiceChannel) {
            return;
        }
        this._voiceChannel = voiceChannel;
        let conn = await this._voiceChannel.join()
        console.log("Joined " + voiceChannel.name + " (voice)");
        this._voiceConn = conn;
    }

    /**
     * Searches the voice channels of the message's server and returns
     * the voicechannel of the author. If author is not on voice channels in
     * the server, null is returned
     * @param message The Discord.js message object that contains the command
     * @returns The Discord.js voice channel object or null, if channel was not found
     */
    findSenderVoiceChannel(message) {
        let foundChannel = null;
        let voiceChannels = message.guild.channels.filter((channel, key) => channel.type === "voice");
        voiceChannels.forEach((vchannel, ckey) => {
            vchannel.members.forEach((member, mkey) => {
                if (member.user === message.author) {
                    foundChannel = vchannel;
                }
            });
        });
        return foundChannel;
    }

    /**
     * Leaves any voice voice channels the client was connected to
     */
    leaveVoiceChannel() {
        let connections = this.voiceConnections.array();
        if (connections.length > 0) {
            connections.forEach(conn => {
                conn.disconnect();
            });
            this._voiceChannel = null;
            this._voiceConn = null;
        }
    }

    /**
     * @returns Is the bot connected to a voice channel
     */
    isVoiceConnected() {
        return this._voiceConn !== null;
    }

    /**
     * @param message Discord.js message object to check
     * @param role The role to check for as a plain string (case sensitive)
     * @returns Boolean if user has specified role
     */
    senderHasRole(message, role) {
        if (message.guild === null) {
            return false;
        }
        let members = message.guild.roles.find(x => x.name === role).members;
        return members.some(member => member.user === message.author);
    }

    /**
     * The first word after the initial command is the target user name,
     * the rest of the message is the payload to be spammed. Message is repeated
     * 5 times, with the target getting pinged every time
     * @param message The whole message object that contains the command
     */
    spamMessage(message) {
        let args = message.content.split(" ");
        let commandIdx = args.findIndex(x => x === "!spam");
        let target = args[commandIdx + 1];
        let spammessage = args[commandIdx + 2];

        if (spammessage === undefined) {
            spammessage = "daPepe";
        } else {
            spammessage = "";

            for (let i = commandIdx + 2; i < args.length; i++) {
                spammessage += args[i] + " ";
            }
        }

        for (let i = 0; i < 5; i++) {
            this._pingTarget(message.channel, target, spammessage);
        }
    }

    /**
     * @returns Discord.js voice connection object of
     * current connected voice channel
     */
    getVoiceConnection() {
        return this._voiceConn;
    }

    /**
     * @returns Discord.js voice channel object of
     * current connected voice channel
     */
    getVoiceChannel() {
        return this._voiceChannel;
    }

    /**
     * Internal command to send a ping to a user
     * @param channel Channel to send the ping in
     * @param target Target user that is pinged
     * @param message Message that is appended to the ping
     */
    _pingTarget(channel, target, message) {
        let pingTarget = channel.members.find(x => x.user.username === target);
        if (pingTarget === null) {
            return;
        }
        channel.send(message, {
            reply: pingTarget
        });
    }

    /**
     * Easy function to bind chat commands with actions
     * @param command The command string that is to be binded
     * @param callback The callback function to be called. It is
     * passed the message object as a parameter
     * @param roleToCheck The role user must have to execute the command.
     * if omitted, no role check is performed
     */
    bindCommand(command, callback, roleToCheck = null) {
        this.on("message", message => {
            if (message.content.includes(command) && !message.author.bot) {
                if (roleToCheck !== null &&
                    !this.senderHasRole(message, roleToCheck)) {
                    return;
                }
                callback(message);
            }
        });
    }

    /**
     * Kills all binded commands and functions. Useful when recreating a client
     */
    unbindCommands() {
        this.removeAllListeners();
    }
}