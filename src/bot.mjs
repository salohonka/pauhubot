// Byno 2018
// Contact me on Discord @ Byno#9236
"use strict";
import * as fs from "fs";
import * as cp from "child_process";
import {
    EventEmitter
} from "events";
import Youtube from "./youtube";
import Client from "./clienttools.mjs";

/**
 * Reads file that has Discord & Youtube API keys in them and initializes
 * the clients. If reading fails, the process is killed
 * @param discordClient Discord.js client or clienttools DiscordClient
 * @param youtubeClient youtube object (See youtube.mjs)
 */
function readAuthFile(discordClient, youtubeClient) {
    try {
        fs.readFile("auth.json", (err, data) => {
            if (err !== null) {
                console.log(err.message);
                process.exit(1);
            }
            let json = JSON.parse(data.toString());
            let discordAuth = json.discord
            let youtubeAuth = json.youtube;

            discordClient.login(discordAuth);
            youtubeClient.login(youtubeAuth);
        });
    } catch (e) {
        console.log(e);
        process.exit(1);
    }
}

/**
 * Simulates a World of Warcraft character on EU realms. First arg is the
 * character name, second arg is the realm name (multi-part names with hyphens)
 * and the last part is if weights are to be included in the sim. Char and realm name
 * are not case sensitive. Implemented with SimC CLI and Node.js exec
 * @param message The Discord.js message object containing the command
 */
function simulate(message) {
    // Parse the command
    let args = message.content.split(" ");
    let char = args[1];
    let realm = args[2];
    let weights = false;
    let executable = process.platform === "win32" ? "simc.exe" : "./simc";
    let dirSeparator = process.platform === "win32" ? "\\" : "/";
    let cmdSeparator = process.platform === "win32" ? "&&" : ";";

    let simC = `cd ..${dirSeparator}SimC ${cmdSeparator} ${executable} armory=eu,${realm},${char} json2=output.json report_details=0 iterations=10000`;
    let outputPath = `..${dirSeparator}SimC${dirSeparator}output.json`;

    // Determine if weights are calculated, if yes then append the exec command
    if (args.includes("weights")) {
        weights = true;
        simC = String.prototype.concat(simC, " calculate_scale_factors=1");
        message.channel.send("Simming with weights, this will take a while...");
    } else {
        message.channel.send("Simming...");
    }

    // Exec the sim as a child process
    let sim = cp.exec(simC,
        (error, stdout, stderr) => {
            if (error !== null) {
                console.log(error.message);
            }
        });

    sim.on("exit", (code, signal) => {
        if (code !== 0) {
            message.channel.send("Error simming, check character name & realm!");
            return;
        }

        // Read the sim output
        fs.readFile(outputPath, (err, data) => {
            if (err !== null) {
                console.log(err.message);
            }
            let json = JSON.parse(data.toString());
            let dpsdata = json.sim.players[0].collected_data.dps

            // Form the chat output message with nice formatting
            let channelMessage =
                `${char}, ${realm}, ${json.sim.players[0].specialization}, 10k iterations:` +
                "\`\`\`\n" + "Median".padEnd(8) + dpsdata["median"].toString().split(".")[0] +
                "\n" + "Mean".padEnd(8) + dpsdata["mean"].toString().split(".")[0];

            // Weights included: append the weights with formatting
            if (weights) {
                channelMessage = String.prototype.concat(channelMessage, "\n\nStat weights:");
                // Iterate each weight and do text formatting
                for (let key in json.sim.players[0].scale_factors) {
                    channelMessage = channelMessage.concat(channelMessage,
                        `\n${key.toString().padEnd(8)}${json.sim.players[0]
                            .scale_factors[key].toString().slice(0, 5)}`
                    );
                }
            }

            channelMessage = String.prototype.concat(channelMessage, "\`\`\`");
            message.channel.send(channelMessage);
        });
    });
}

/**
 * Prints all the commands available to use to the channel that the command
 * was sent from
 * @param message The Discord.js message object that triggered the callback
 */
function printHelp(message) {
    let msg = "\`\`\`\n" +
        "Command".padEnd(11) + "Users".padEnd(11) + "Params" + "\n" +
        "!help".padEnd(11) + "All".padEnd(11) + "\n" +
        "!Commands".padEnd(11) + "All".padEnd(11) + "\n" +
        "!voice".padEnd(11) + "Lahna".padEnd(11) + "me or channel name" + "\n" +
        "!leave".padEnd(11) + "Lahna".padEnd(11) + "\n" +
        "!stop".padEnd(11) + "Lahna".padEnd(11) + "\n" +
        "!skip".padEnd(11) + "Requester".padEnd(11) + "\n" +
        "!queue".padEnd(11) + "All".padEnd(11) + "\n" +
        "!sr".padEnd(11) + "All".padEnd(11) + "Youtube search term" + "\n" +
        "!stuck".padEnd(11) + "Requester".padEnd(11) + "\n" +
        "!requester".padEnd(11) + "All\`\`\`";
    message.channel.send(msg);
}

/**
 * Binds all the various commands. Formed into a function to easily
 * rebind all the commands in case of a client crash
 * @param discordClient Discord.js client or DiscordClient
 */
function bindCommands(discordClient) {
    // Voice channel stuff
    discordClient.bindCommand("!voice", async (message) => {
        try {
            await discordClient.joinVoiceChannelFromMessage(message)
            youtube.setVoiceConnection(discordClient.getVoiceConnection());
        } catch (e) {
            message.channel.send("Failed to join channel: " + e);
        }
    }, "Lahna");
    discordClient.bindCommand("!leave", () => {
        youtube.setVoiceConnection(null);
        discordClient.leaveVoiceChannel();
    }, "Lahna");
    console.log("Binded voice channel commands...");

    // Songrequets and music stuff
    discordClient.bindCommand("!stop", message => {
        message.channel.send("Cleared songqueue");
        youtube.clearQueue();
    }, "Lahna");
    discordClient.bindCommand("!skip", message => {
        if (discordClient.senderHasRole(message, "Lahna") ||
            youtube.isRequester(message.author)) {
            youtube.skip();
        }
    });
    discordClient.bindCommand("!queue", message =>
        message.channel.send(youtube.getQueue()));
    discordClient.bindCommand("!sr", message => {
        if (discordClient.isVoiceConnected()) {
            if (discordClient.findSenderVoiceChannel(message) ===
                discordClient.getVoiceChannel()) {
                try {
                    youtube.search(message);
                } catch (e) {
                    console.log(e);
                    message.channel.send(e);
                }
            } else {
                message.channel.send("Requester must be in the voice channel");
            }
        }
    });
    discordClient.bindCommand("!stuck", async (message) => {
        if (youtube.isRequester(message.author)) {
            youtube.setVoiceConnection(null, false);
            discordClient.leaveVoiceChannel();

            let chan = discordClient.findSenderVoiceChannel(message);
            await discordClient.joinVoiceChannel(chan)
            youtube.setVoiceConnection(discordClient.getVoiceConnection(), false);
        }
    });
    discordClient.bindCommand("!requester", message => {
        let requester = youtube.getRequester();
        message.channel.send(
            requester !== null ? requester.username : "Audio isn't playing"
        );
    });
    console.log("Binded Youtube commands...");

    // Miscellaneous stuff
    discordClient.bindCommand("!sim", simulate);
    discordClient.bindCommand("!spam", discordClient.spamMessage, "Lahna");
    discordClient.on("message", message => {
        let privateMessage = message.channel.type === "dm";
        // log private message
        if (privateMessage) {
            console.log("--- DM FROM USER " + message.author.username +
                '\n--- "' + message.content + '"');
        }
    });
    discordClient.bindCommand("!help", printHelp);
    discordClient.bindCommand("!commands", printHelp);
    discordClient.on("voiceStateUpdate", (oldMember, newMember) => {
        // User that requsted a song leaves the channel -> skip the song
        if (oldMember.voiceChannel === discordClient.getVoiceChannel() &&
            newMember.voiceChannel !== discordClient.getVoiceChannel() &&
            youtube.isRequester(oldMember.user)) {
            console.log("Requester left, skipping");
            youtube.skip();
        }
    });
    discordClient.bindCommand("!reset", () => {
        youtube.clearQueue();
        resetClients(discordClient, youtube);
    }, "Lahna");

    discordClient.on("error", error => {
        console.log("##### Error encountered, reseting discordClient");
        console.log(error.message);
        resetClients(discordClient, youtube);
    });
    console.log("Binded miscellaneous commands...");
}

/**
 * Overwrites both connection objects and attempts to rejoin
 * the voice channel the bot was active before the reset.
 * Called usually when an error is encountered and connection
 * needs to be reset
 * @param discordClient Discord.js or DiscordClient object
 * @param youtube YoutubeConnection object (see youtube.mjs)
 */
async function resetClients(discordClient, youtube) {
    let oldVoiceChannel = discordClient.getVoiceChannel();
    discordClient.unbindCommands();
    await discordClient.destroy();
    discordClient = new Client();
    youtube = new Youtube();
    readAuthFile(discordClient, youtube);
    bindCommands(discordClient);
    discordClient.joinVoiceChannel(oldVoiceChannel);
    console.log("Clients reset");
}

console.log("Initializing bot, will print \"PauhuBot ready\" when ready!");
EventEmitter.prototype.setMaxListeners(20);
console.log("Creating clients...");
let client = new Client();
let youtube = new Youtube();
console.log("Clients created, logging in...");
readAuthFile(client, youtube);
console.log("Logged in, binding commands...");
bindCommands(client);