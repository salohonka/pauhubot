import Utubestream from "youtube-audio-stream";
import Utubesearch from "youtube-search";
import * as query from "querystring";
import {
    get
} from "https";

export default class YoutubeConnection {
    constructor() {
        this._voiceConn;
        this._soundPlaying;
        this._songRequester;
        this._soundStream;
        this._songQueue = [];
        this._searchOptions = {
            maxResults: 1,
            key: "",
            type: "video",
            part: ""
        };
        this._specificSearchOptions = {
            key: "",
            part: "contentDetails",
            id: ""
        };
        this._songTimeout = null;

        this.login = this.login.bind(this);
        this._play = this._play.bind(this);
        this.search = this.search.bind(this);
        this.skip = this.skip.bind(this);
        this.isRequester = this.isRequester.bind(this);
        this.clearQueue = this.clearQueue.bind(this);
        this.getQueue = this.getQueue.bind(this);
        this.setVoiceConnection = this.setVoiceConnection.bind(this);
        this._searchCallback = this._searchCallback.bind(this);
        this._pushSongQueue = this._pushSongQueue.bind(this);
        this.getRequester = this.getRequester.bind(this);
        this._setSongTimeout = this._setSongTimeout.bind(this);
        this._cancelSongTimeout = this._cancelSongTimeout.bind(this);
        this._specificQuery = this._isAgeRestricted.bind(this);
    }

    /**
     * "Login" with the Youtube API-key to allow seaches
     * @param auth API-key
     */
    login(auth) {
        this._searchOptions.key = auth;
        this._specificSearchOptions.key = auth;
    }

    /**
     * Internal command to play a Youtube stream from URL
     * @param channel Discord.js message channel
     * @returns The readable audiostream of the Youtube video
     */
    _play(channel = null) {
        if (this._voiceConn === null) {
            this.clearQueue();
            return;
        }

        let song = this._songQueue.shift();
        // If requester left, skipping over song can lead to empty queue
        if (!song) {
            this._soundPlaying = false;
            this._soundStream.end();
            return;
        }
        
        this._songRequester = song.requester;
        let requesterPresent = this._voiceConn.channel.members
            .array().some(elem => elem.user.id === this._songRequester.id);

        if (requesterPresent && channel) {
            channel.send(`Playing ${song.name} (Requested by ${song.requester.username})`);

        } else if (!requesterPresent) {
            if (channel) {
                channel.send("Requester not present, skipping " + song.name);
            }
            console.log("requester not present, shift & play next");
            this._songQueue.shift();
            this._play(channel);
            return;
        }

        // Form an audio stream and play it
        try {
            let stream = Utubestream("https://www.youtube.com/watch?v=" + song.id);
            this._soundStream = this._voiceConn.playStream(stream);
            this._setSongTimeout(15, channel);
        } catch (err) {
            throw "Error reading youtube stream";
        }

        // Songrequest started playing, set playing flags
        this._soundPlaying = true;

        // Callback to play next song in queue or clear playing flags
        this._soundStream.on("end", () => {
            if (this._voiceConn === null) {
                this._soundPlaying = false;
                this._songRequester = null;

            } else {
                if (this._songQueue.length > 0) {
                    try {
                        this._play(channel);

                    } catch (e) {
                        throw e;
                    }

                } else {
                    this._soundPlaying = false;
                    this._songRequester = null;
                }
            }
        });
    }

    /**
     * Parses the command, stripping the initial command part
     * and searches & play Youtube for the rest of the command
     * @param message The whole command as one string
     */
    search(message) {
        Utubesearch(
            message.content.slice(4),
            this._searchOptions,
            async (error, results) => {
                if (error || results.length === 0) {
                    console.log(error || "empty results");
                    message.channel.send("Could not find song!");
                    return;
                }
                try {
                    if (await this._isAgeRestricted(results[0].id)) {
                        message.channel.send("Can't play age restricted song!");
                        return;
                    } else {
                        this._searchCallback(results, message);
                    }

                } catch (e) {
                    throw e;
                }
            }
        );
    }

    /**
     * Skip the current song in the songqueue
     */
    skip() {
        this._soundStream.end();
    }

    /**
     * @returns True if the name matches the current songrequester
     */
    isRequester(name) {
        return name === this._songRequester;
    }

    /**
     * Stops any currently playing audio and clears the songqueue
     */
    clearQueue() {
        if (this._songQueue.length > 0) {
            this._songQueue = [];
        }
        if (this._soundStream !== null && this._soundStream !== undefined) {
            this._soundStream.end();
        }
    }

    /**
     * @returns The current songqueue as a string, preformatted for Discord
     */
    getQueue() {
        let queuemessage;
        if (this._songQueue.length > 0) {
            queuemessage = "\`\`\`";
            this._songQueue.forEach(song => {
                queuemessage = String.prototype.concat(queuemessage, `${song.requester.username}: ${song.name}\n`);
            });
            queuemessage = String.prototype.concat(queuemessage, "\`\`\`");
        } else {
            queuemessage = "The queue is empty!";
        }

        return queuemessage;
    }

    /**
     * Set the voicechannel connection to allow sound playback
     * @param connection Discord.js voice connection object, use null to reset the connection
     * @param resetQueue Should the existing audio queue be reset or not (default is true)
     */
    setVoiceConnection(connection, resetQueue = true) {
        this._voiceConn = connection;
        if (connection === null && resetQueue) {
            this.clearQueue();
        }

        if (connection !== null && this._songQueue.length > 0) {
            this._play();
        }
    }

    /**
     * Internal callback function to add a song to the songqueue.
     * If the queue is empty, the request will play immediately
     * @param results The youtube-search result object
     * @param message The message that contained the songrequest command
     */
    _searchCallback(results, message) {
        this._pushSongQueue(results[0], message.author);

        if (!this._soundPlaying) {
            try {
                this._play(message.channel);
            } catch (e) {
                throw e;
            }

        } else {
            message.channel.send("Added " + results[0].title + " to song queue");
        }
    }

    /**
     * Forms an object containing the request information and pushes
     * it to the songqueue
     * @param result A single member of the youtube-search result object
     * @param author Song requestee
     */
    _pushSongQueue(result, author) {
        this._songQueue.push({
            name: result.title,
            id: result.id,
            requester: author
        });
    }

    /**
     * @returns The requester of the currently playing audio. If audio isn't playing,
     * returns null
     */
    getRequester() {
        if (this._soundPlaying) {
            return this._songRequester;
        } else {
            return null;
        }
    }

    /**
     * Set a timeout when the current request will be cancelled
     * @param minutes Amount of minutes until songrequest is cut off
     * @param channel A text channel where a timeout message notification is sent
     * when timeout is reached
     */
    _setSongTimeout(minutes, channel) {
        this._cancelSongTimeout();
        this._songTimeout = setTimeout(() => {
            if (this._soundPlaying) {
                this.skip();
                if (channel !== null) {
                    channel.send(`Song timeout (${minutes} minutes) reached`);
                }
            }
        }, 1000 * 60 * minutes);
    }

    /**
     * Cancel the current song timeout
     */
    _cancelSongTimeout() {
        if (this._songTimeout !== null) {
            clearTimeout(this._songTimeout);
        }
    }

    /**
     * Query youtube specific search with video id to see
     * if video is age restricted. The bot cannot play age
     * restricted videos
     * @param id Youtube video id, usually comes from a
     * youtube-search query
     * @returns Boolean: True for age restricted content
     */
    async _isAgeRestricted(id) {
        this._specificSearchOptions.id = id;
        // Wrap in promise to allow await, standard node
        // requests suck, they don't return promises by default
        return new Promise((resolve, reject) => {
            try {
                get('https://www.googleapis.com/youtube/v3/videos?' + query.stringify(this._specificSearchOptions),
                    (resp) => {
                        let data = null;
                        let response;

                        // Gather the incoming data
                        resp.on("data", received => {
                            data += received;
                        });

                        // Request ended, parse data
                        resp.on("end", () => {
                            data = data.slice(4);
                            response = JSON.parse(data);
                            let item = response.items[0];

                            // Try and see if the response for video has restriction tags
                            if (!item.contentDetails.hasOwnProperty("contentRating")) {
                                resolve(false);

                            } else if (item.contentDetails.contentRating.hasOwnProperty("ytRating") &&
                                item.contentDetails.contentRating.ytRating === "ytAgeRestricted") {
                                resolve(true);
                            }

                            // Fault tolerance
                            resolve(true);
                        });
                    }
                );
            } catch (e) {
                // In case of request error, just deny video
                resovle(true);
            }
        });
    }
}