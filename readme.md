# PauhuBot
Matias "Byno" Salohonka, 2018


## How to run:
0. Install [node.js](https://blog.teamtreehouse.com/install-node-js-npm-windows), [yarn](https://yarnpkg.com/lang/en/docs/install/) and [ffmpeg](https://github.com/adaptlearning/adapt_authoring/wiki/Installing-FFmpeg), make sure that ffmpeg is in your path
1. `git clone https://gitlab.com/salohonka/pauhubot.git`
2. `cd pauhubot`
3. `yarn`
4. Create a `auth.json` file in `src/`
5. Put your discord and youtube API keys in the json, the format is at the bottom
6. (optional) Add SimulationCraft support (see the section below)
7. `cd src`
8. `node --experimental-modules bot.mjs`

Node has to be given that flag to run native ES modules (the `import * as fs from "fs"` stuff).


## What does it do:
For the functionality, ask the bot (!help or !commands)! The code has comments and jsdocs, but common commands are:

+ !sim
+ !sr
+ !voice
+ !spam


## auth.json format
```
{
    "discord": "YOUR-DISCORD-KEY-HERE",
    "youtube": "YOUR-YOUTUBE-KEY-HERE"
}
```


## SimulationCraft support

Create `SimC/` and place the executable in there.

The simplest way to add simulation support is to [download a prebuilt windows binary](https://simulationcraft.org/download.html) and extract it so that simc.exe is in `SimC/simc.exe`.

Linux support is possible, but harder: you must build the executable yourself. SimulationCraft wiki has a [guide how to do that](https://github.com/simulationcraft/simc/wiki/HowToBuild#command-line-interface). You also need to get your own Blizzard API key if you don't use a prebuilt binary, the wiki also has a [guide how to do that](https://github.com/simulationcraft/simc/wiki/BattleArmoryAPI). Note that for the CLI you must place the apikey in `apikey.txt` in the same folder as the executable.